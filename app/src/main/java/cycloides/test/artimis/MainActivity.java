package cycloides.test.artimis;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Roshan on 2/5/2018.
 */
public class MainActivity extends BaseActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,View.OnClickListener {

    private static final int PERMISSION_REQUEST_CODE = 200;
    double latitude,longitude;
    GoogleMap map;
    String address; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
    String city;
    String state;
    String country;
    String postalCode;
    String knownName; // Only if available else return NULL
    SupportMapFragment mapFragment;
    int CAMERA_PIC_REQUEST=1;
    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private Button take_picture;

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Used to initialize view
        initializeFun();
        //Used to enable GPS location
        checkGPS(MainActivity.this);
        //checking permission request for accessing location
        checkLocationPermistionFun();

        take_picture.setOnClickListener(this);

        //API Client setup
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) MainActivity.this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(LocationServices.API)
                .build();

        //Inflating Map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void initializeFun() {
        take_picture=(Button)findViewById(R.id.take_picture_id);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * This function is used for checking Location permission on run time
     */
    private void checkLocationPermistionFun() {

        if (!checkPermission()) {

            requestPermission();


        }

    }

    private boolean checkPermission() {

        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_COARSE_LOCATION);

        return ((result1 == PackageManager.PERMISSION_GRANTED) && (result2 == PackageManager.PERMISSION_GRANTED));
    }

    /**
     * This function is used for requesting location permission to get enabled by User
     */
    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map=googleMap;
//        map.setMyLocationEnabled(true);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length >= 0) {
                    Log.d("GGGG", "CCCCC");

                    boolean Accepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (Accepted) {
                        /*new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent=new Intent(MainActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        },3000);*/
                        Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        }, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                finish();
                                            }
                                        });
                                return;
                            } else {
                                finish();
                            }
                        }

                    }
                }


                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener okListener_No) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You need to allow access permissions");
        builder.setCancelable(false);
        builder.setNegativeButton("No", okListener_No);

        builder.setPositiveButton("Yes", okListener);

        AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(alert.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#FF205C2F"));
        alert.getButton(alert.BUTTON_POSITIVE).setTextColor(Color.parseColor("#FF205C2F"));


    }


    /**
     *
     * Get Current Location
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        @SuppressLint("MissingPermission") Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            // Blank for a moment...
            Toast.makeText(MainActivity.this,"Current location is not fetched",Toast.LENGTH_LONG).show();
        }
        else {
            handleNewLocation(location);
        }
    }

    /**
     * This function is used to handle current location
     * @param location contain current location
     */
    private void handleNewLocation(Location location) {

        latitude=location.getLatitude();
        longitude=location.getLongitude();
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //address function
        addressGetFun();
        map.addMarker(new MarkerOptions()
                .position((new LatLng(latitude, longitude)))
                .title(knownName)
                .snippet(address));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(latitude, longitude), 15 ));
       // Log.d("JJJ", latitude+" inside"+longitude);
    }


    /**
     * This function is used to convert latitude and longitude to Address
     */
    private void addressGetFun() {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        address = addresses.get(0).getAddressLine(0);
        city = addresses.get(0).getLocality();
        state = addresses.get(0).getAdminArea();
        country = addresses.get(0).getCountryName();
        postalCode = addresses.get(0).getPostalCode();
        knownName = addresses.get(0).getFeatureName();
        //Log.d("OOO","ADDR "+address+" CITY"+city+" STATE"+state+"COUNTRY "+country+"POSTAL CODE"+postalCode+"KNOWNNAME"+knownName+" ");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("JJJ","2");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("JJJ", "3");

         /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }

    /**
     *
     * click events for all views in this activity
     */

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            /*
            camera click button
            */
            case R.id.take_picture_id:
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CAMERA_PIC_REQUEST)
        {
            Bitmap image=(Bitmap)data.getExtras().get("data");

        }
    }
}
